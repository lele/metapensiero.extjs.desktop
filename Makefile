# -*- coding: utf-8 -*-
# :Project:   metapensiero.extjs.desktop
# :Created:   mer 12 dic 2012 10:54:44 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2012, 2013, 2014, 2016, 2018, 2020, 2021 Lele Gaifax
#

PYTHON := $(BINDIR)python
DOMAIN := mp-desktop
SRCDIR := src/metapensiero/extjs/desktop
BUMPER := $(BINDIR)bump_version
VERSION_TXT := version.txt
VERSION = $(shell cat $(VERSION_TXT))
POs := $(wildcard $(SRCDIR)/locale/*/LC_MESSAGES/*.po)
MOs := $(patsubst %.po,%.mo,$(POs))
PACKAGE_NAME = $(shell $(PYTHON) setup.py --name)
TWINE := $(BINDIR)twine

%.mo: %.po
	$(PYTHON) setup.py compile_catalog

.PHONY: compile-catalogs
compile-catalogs: $(MOs)

.PHONY: update-catalogs
update-catalogs:
	$(PYTHON) setup.py extract_messages update_catalog

.PHONY: bump-minor-version
bump-minor-version:
	$(BUMPER) $(VERSION_TXT)

.PHONY: bump-major-version
bump-major-version:
	$(BUMPER) -f major $(VERSION_TXT)

.PHONY: assert-clean-tree
assert-clean-tree:
	@(test -z "$(shell git status -s --untracked=no)" || \
	  (echo "UNCOMMITTED STUFF" && false))

.PHONY: assert-master-branch
assert-master-branch:
	@(test "$(shell git rev-parse --abbrev-ref HEAD)" = "master" || \
	  (echo "NOT IN MASTER BRANCH" && false))

.PHONY: release
release: assert-master-branch assert-clean-tree
	$(MAKE) bump-minor-version update-catalogs
	@echo ">>>"
	@echo ">>> Do your duties (update CHANGES.rst for example), then"
	@echo ">>> execute “make tag-release”."
	@echo ">>>"

.PHONY: major-release
major-release: assert-master-branch assert-clean-tree
	$(MAKE) bump-major-version update-catalogs
	@echo ">>>"
	@echo ">>> Do your duties (update CHANGES.rst for example), then"
	@echo ">>> execute “make tag-release”."
	@echo ">>>"

.PHONY: check-long-description-markup
check-long-description-markup:
	@$(PYTHON) setup.py --quiet sdist
	@$(TWINE) check dist/$(PACKAGE_NAME)-$(VERSION).tar.gz

.PHONY: check-release-date
check-release-date:
	@fgrep -q "$(VERSION) ($(shell date --iso-8601))" CHANGES.rst \
	  || (echo "ERROR: release date of version $(VERSION) not set in CHANGES.rst"; exit 1)

.PHONY: tag-release
tag-release: assert-master-branch check-release-date check-long-description-markup
	git commit -a -m "Release $(VERSION)"
	git tag -a -m "Version $(VERSION)" v$(VERSION)

.PHONY: pypi-upload
pypi-upload: assert-master-branch assert-clean-tree compile-catalogs
	$(PYTHON) setup.py sdist bdist_wheel
	$(TWINE) upload dist/$(PACKAGE_NAME)-$(VERSION).tar.gz dist/$(PACKAGE_NAME)-$(VERSION)-*.whl

.PHONY: publish
publish: pypi-upload
	git push
	git push --tags
