# -*- coding: utf-8 -*-
# :Project:   metapensiero.extjs.desktop -- Test ExtJS deps extraction
# :Created:   mar 17 lug 2018 13:10:03 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018, 2022 Lele Gaifax
#

from collections import deque
from importlib.resources import files
from os import fspath
from pathlib import Path

from metapensiero.extjs.desktop.scripts.extjs_deps import Class

import pytest


EXTJS_SRC_DIR = files('metapensiero.extjs.desktop') / 'assets/extjs/src'


def test_get_class_script():
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    prefix_map = {'Foo': '/tmp/foo/'}
    Class.aliases = {'Foo.bar.Ham': 'Foo.bar.Spam'}

    assert gcs('Foo.bar.Spam', prefix_map) == '/tmp/foo/bar/Spam.js'
    assert gcs('Foo.bar.Ham', prefix_map) == '/tmp/foo/bar/Spam.js'


@pytest.mark.parametrize('script,class_names', (
    ('''Ext.define('FooBar', {});''',
     {'FooBar'}),
    ('''Ext.define('Foo', {}); Ext.define('Bar', {extend: 'Foo'});''',
     {'Foo', 'Bar'}),
))
def test_extract_script_classes(tmpdir, script, class_names):
    from metapensiero.extjs.desktop.scripts.extjs_deps import _extract_script_classes as esc

    tempdir = Path(tmpdir)
    Class.aliases = {}

    scriptfn = tempdir / 'script.js'
    scriptfn.write_text(script)

    assert {c.name for c in esc(fspath(scriptfn), deque(), {})} == class_names


def test_extract_script_classes_util_History():
    from metapensiero.extjs.desktop.scripts.extjs_deps import _extract_script_classes as esc
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    prefix_map = {'Ext': fspath(EXTJS_SRC_DIR) + '/'}
    Class.aliases = {}

    script = gcs('Ext.util.History', prefix_map)
    queue = deque()
    result = esc(script, queue, prefix_map)
    assert len(result) == 1
    cls = result[0]
    assert cls.name == 'Ext.util.History'
    assert cls.mixins == ['Ext.util.Observable']
    assert cls.aliases['Ext.History'] == 'Ext.util.History'


def test_extract_script_classes_util_Observable():
    from importlib.resources import path
    from metapensiero.extjs.desktop.scripts.extjs_deps import _extract_script_classes as esc
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    prefix_map = {'Ext': fspath(EXTJS_SRC_DIR) + '/'}
    Class.aliases = {}

    script = gcs('Ext.util.Observable', prefix_map)
    queue = deque()
    result = esc(script, queue, prefix_map)
    assert len(result) == 1
    cls = result[0]
    assert cls.name == 'Ext.util.Observable'
    assert cls.superclass is None
    assert cls.uses is None
    assert cls.requires == ['Ext.util.Event', 'Ext.EventManager']


def test_extract_script_classes_dom_Helper():
    from metapensiero.extjs.desktop.scripts.extjs_deps import _extract_script_classes as esc
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    prefix_map = {'Ext': fspath(EXTJS_SRC_DIR) + '/'}
    Class.aliases = {}

    script = gcs('Ext.dom.Helper', prefix_map)
    queue = deque()
    result = esc(script, queue, prefix_map)
    assert len(result) == 1
    cls = result[0]
    assert cls.name == 'Ext.dom.Helper'
    assert cls.superclass == 'Ext.dom.AbstractHelper'
    assert cls.uses is None
    assert cls.requires == ['Ext.dom.AbstractElement']


def test_extract_script_classes_fx_Easing():
    from metapensiero.extjs.desktop.scripts.extjs_deps import _extract_script_classes as esc
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    prefix_map = {'Ext': fspath(EXTJS_SRC_DIR) + '/'}
    Class.aliases = {}

    script = gcs('Ext.fx.Easing', prefix_map)
    queue = deque()
    result = esc(script, queue, prefix_map)
    assert len(result) == 1
    cls = result[0]
    assert cls.name == 'Ext.fx.Easing'
    assert cls.superclass is None
    assert cls.uses is None
    assert cls.requires == ['Ext.fx.CubicBezier']


@pytest.mark.parametrize('scripts,ordered_class_names', (
    ((("Foo.C1", """Ext.define('Foo.C1', {extend: 'Foo.C0'});"""),
      ("Foo.C2", """Ext.define('Foo.C2', {extend: 'Foo.C1'});"""),
      ("Foo.C0", """Ext.define('Foo.C0', {});"""),
      ("Foo.Z", """Ext.define('Foo.Z', {});""")),
     ("Foo.C0", "Foo.Z", "Foo.C1", "Foo.C2")),
))
def test_get_needed_sources(tmpdir, scripts, ordered_class_names):
    from metapensiero.extjs.desktop.scripts.extjs_deps import get_needed_sources as gns
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    tempdir = Path(tmpdir)

    foodir = tempdir / 'foo'
    foodir.mkdir()

    prefix_map = {'Foo': fspath(foodir) + '/'}
    Class.aliases = {}

    c2s = {}
    for classname, script in scripts:
        assert gcs(classname, prefix_map)
        scriptfn = (foodir / classname.split('.')[-1]).with_suffix('.js')
        scriptfn.write_text(script)
        c2s[classname] = fspath(scriptfn)

    result = gns([s[0] for s in scripts], prefix_map)
    assert result == [c2s[c] for c in ordered_class_names]


@pytest.mark.parametrize('scripts,ordered_class_names', (
    ((("Foo.C", """Ext.define('Foo.C', {extend: 'Ext.app.Application'})"""),),
     ("Ext.app.Application", "Foo.C")),
))
def test_get_needed_sources_extjs(tmpdir, scripts, ordered_class_names):
    from metapensiero.extjs.desktop.scripts.extjs_deps import get_needed_sources as gns
    from metapensiero.extjs.desktop.scripts.extjs_deps import _get_class_script as gcs

    tempdir = Path(tmpdir)

    foodir = tempdir / 'foo'
    foodir.mkdir()

    prefix_map = {'Ext': fspath(EXTJS_SRC_DIR) + '/',
                  'Foo': fspath(foodir) + '/'}
    Class.aliases = {}

    for classname, script in scripts:
        scriptfn = (foodir / classname.split('.')[-1]).with_suffix('.js')
        scriptfn.write_text(script)

    bundle = fspath(files('metapensiero.extjs.desktop') / 'assets/extjs/ext-dev.js')
    result = gns([s[0] for s in scripts], prefix_map, bundle)
    prev = -1
    for c in ordered_class_names:
        index = result.index(gcs(c, prefix_map))
        assert index > prev, "Class %s out of order!" % c
        prev = index
