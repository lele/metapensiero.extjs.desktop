# -*- coding: utf-8 -*-
# :Project:   metapensiero.extjs.desktop -- Tests on the minifier module
# :Created:   dom 17 mar 2013 11:45:26 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2012, 2013, 2016, 2018 Lele Gaifax
#

from os import fspath
from pathlib import Path


main_css = """
@import 'http://localhost/foo.css';
@import 'a.css';
.users-icon {
    background-image: url('../images/icon.png') ! important;
}
"""

a_css = """
@import '../b.css';
@import 'file://foobar/some/path.css';
.users-shortcut {
    background-image: url('/images/shortcut.png') ! important;
}
"""

b_css = """
.help-icon {
    background-image: url('../modules/help.png') !important;
}
"""


def test_resolve_css_imports(tmpdir):
    from metapensiero.extjs.desktop.scripts.minifier import resolve_css_imports

    tempdir = Path(tmpdir)

    cssdir = tempdir / 'css'
    cssdir.mkdir()

    main = cssdir / 'main.css'
    main.write_text(main_css)

    (cssdir / 'a.css').write_text(a_css)
    (tempdir / 'b.css').write_text(b_css)

    result = resolve_css_imports(fspath(main), {'/xxx/': fspath(tmpdir) + '/'})

    assert "@import 'http://localhost/foo.css';" in result
    assert "@import 'file://foobar/some/path.css';" in result
    assert "@import 'a.css'" not in result
    assert "/modules/help.png'" in result
    assert "'/images/shortcut.png'" in result
    assert "'/xxx/images/icon.png'" in result
