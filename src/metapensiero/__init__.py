# -*- coding: utf-8 -*-
# :Project:   metapensiero.extjs.desktop
# :Created:   mar 11 dic 2012 10:15:21 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2012, 2016 Lele Gaifax
#

# To shut up setuptools warning leave the following string: declare_namespace
from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
